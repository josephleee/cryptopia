# Cryptopia Client & Charting

1. Clone this repo.
2. Create a Python 3 Virtualenv
    `virtualenv --python=python3 venv`
3. Install dependencies and this library in development mode
    `python setup.py develop`
4. Start a Jupyter notebook
    `jupyter notebook --notebook-dir=notebooks --ip=0.0.0.0 --port=8989`
5. Log into the Jupyter Notebook server: `localhost:8989`
6. Create a new Python 3 notebook with the following code. Update to your needs.

```
from datetime import datetime

from cryptopia.client import Cryptopia
from cryptopia.currency import CurrencyHistory
import matplotlib.pyplot as plt

%matplotlib inline


client = Cryptopia(True)
currency = 'Ether'
symbol = 'ETH'
data_range = '1m' # 1 month. Must be one of ['1d', '2d', '1w', '2w', '1m', '3m', '6m', 'all']
interval = 15 # interval length of candles in minutes to pull from cryptopia, suggested min is 15. Otherwise requests take forever
data = client.get_trade_pair_chart(symbol, data_range, interval)

history = CurrencyHistory(data, currency, symbol)
start = datetime(2017, 1, 1)
group = 1440 # Adjust this for candle length, also in minutes. Use a multiple of the interval pulled from the interval length above.
fig = history.plot(group, start=start, satoshis=True, ichimoku=True)
```
