from argparse import ArgumentParser
from collections import OrderedDict
from datetime import datetime
import json
from time import time

import pandas as pd
import requests


data_ranges = ['1d', '2d', '1w', '2w', '1m', '3m', '6m', 'all']


class Cryptopia:
    """
    Cryptopia Exchange API Client
    """

    def __init__(self, prefetch_trade_pairs=False):
        self.base_url = 'https://www.cryptopia.co.nz'
        self.trade_pairs = None
        if prefetch_trade_pairs:
            self.set_trade_pairs('BTC')

    def set_trade_pairs(self, base='BTC'):
        """
        Get data regarding all traded pairs against base currency
        """
        url = '{}/api/GetTradePairs'.format(self.base_url)
        resp = requests.get(url)
        resp.raise_for_status()
        payload = json.loads(resp.content.decode('utf8'),
                             object_pairs_hook=OrderedDict)
        trade_pairs = pd.DataFrame(payload['Data'])
        trade_pairs = trade_pairs[trade_pairs.BaseSymbol == base]
        trade_pairs.set_index('Symbol', inplace=True)
        self.trade_pairs = trade_pairs

    def perform_trade_pair_chart_request(self, trade_pair_id, data_range,
                                         data_group, end=None):
        """
        """
        url = '{}/Exchange/GetTradePairChart'.format(self.base_url)
        params = {
            'tradePairId': trade_pair_id,
            'dataRange': data_range,
            'dataGroup': data_group,
            '_': end
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        return resp.json()

    def get_trade_pair_chart(self, symbol, data_range, data_group, end=None):
        """
        Get candles/volume for trade pair
        :param symbol: abbreviation for currency on exchange, e.g. ETH
        :param data_range: 0-index of
            ['1d', '2d', '1w', '2w', '1m', '3m', '6m', 'all']
        :param data_group: minutes per candle
        :param end: epoch in ms of request, not sure how it's used by server -
            can be treated as superfluous
        """
        if self.trade_pairs is None:
            self.set_trade_pairs()
        if not end:
            end = int(time() * 1000)
        range_index = data_ranges.index(data_range.lower())
        if range_index == -1:
            raise ValueError('Invalid data range: {}'.format(data_range))
        trade_pair_id = self.trade_pairs.loc[symbol].Id
        payload = self.perform_trade_pair_chart_request(
            trade_pair_id, range_index, data_group, end)
        rows = []
        for candle, volume in zip(payload['Candle'], payload['Volume']):
            candle.append(volume['y'])
            candle.append(volume['basev'])
            rows.append(candle)
        df = pd.DataFrame(rows, columns=[
            'time', 'open', 'high', 'low', 'close', 'volume', 'btc_volume'])
        df.set_index(pd.DatetimeIndex(pd.to_datetime(df.time, unit='ms')),
                     inplace=True)
        del df['time']
        return df


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-i', '--interval', required=True, type=int,
                        help='Time interval per bar in minutes')
    spans = ['1d', '2d', '1w', '2w', '1m', '3m', '6m', 'all']
    parser.add_argument('-s', '--span', required=True, choices=spans,
                        help='Time span limit, one of {1d,2d,1w,2w,1m,3m,6m,all}')
    parser.add_argument('currency', nargs=1, help='Currency abbreviation')
    args = parser.parse_args()

    span_index = spans.index(args.span.lower())
    client = Cryptopia()
    # currencies = client.get_currency_summary()
    end = int((datetime.utcnow() -
               datetime.utcfromtimestamp(0)).total_seconds() * 1000)
    data = client.get_trade_pair_chart(args.currency, span_index,
                                       args.interval, end)
    import ipdb; ipdb.set_trace()
