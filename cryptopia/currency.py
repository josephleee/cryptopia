from matplotlib.dates import date2num
from matplotlib.finance import candlestick_ohlc
from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
import pandas as pd

from cryptopia.ichimoku import (
    chikou_span,
    kijun_sen,
    senkou_span_a,
    senkou_span_b,
    tenkan_sen,
)


BAR_WIDTH_RATIO = 0.5
MINS_IN_DAY = 1440
# Ratio of y-value range to use as padding above and below
Y_AXIS_PAD_RATIO = .05
# FIGURE_DPI = 300
# FIGURE_WIDTH = 16
# FIGURE_HEIGHT = 9
FIGURE_WIDTH = 12
FIGURE_HEIGHT = 8
FIGURE_DPI = 300
SATOSHI_FACTOR = 10e7
COLOR_UP = 'green'
COLOR_DOWN = 'red'
COLOR_TENKAN_SEN = '#dd1c77' # red-magenta
COLOR_KIJUN_SEN = '#3182bd' # blue-aquamarine
COLOR_SENKOU_SPAN = 'orange'
COLOR_SENKOU_UP = (0.2, 1., 0.2, 0.2) # green
COLOR_SENKOU_DOWN = (1., 0.2, 0.2, 0.2) # red
COLOR_CHIKOU_SPAN = '#31a354' # green


class CurrencyHistory:

    def __init__(self, data, name, symbol, base='BTC'):
        self.data = data
        self.name = name
        self.symbol = symbol

    @staticmethod
    def time_span(data, start=None, end=None):
        """
        :param data: DataFrame
        :param start: (datetime) moment to being data, default to as far back
            as the data permits
        :param end: (datetime) moment to end data, default to now
        :returns: new DataFrame over given time span
        """
        if start and end:
            mask = (data.index >= start) & (data.index < end)
        elif start and not end:
            mask = data.index >= start
        elif end:
            mask = data.index < end
        else:
            return data.copy()
        return data[mask]

    @staticmethod
    def resample(data, group):
        """
        Update dataset to a coarser interval.
        For example, if data is aggregated over 15 min. intervals, call
        `group(60)` to generate a new dataframe aggregated over 60 min.
        :param group: interval length in minutes
        :returns: new DataFrame
        """
        resampler = data.resample('{}T'.format(group))
        return pd.concat([
            resampler.open.first(),
            resampler.high.max(), resampler.low.min(), resampler.close.last(),
            resampler.btc_volume.sum()], axis=1)

    def plot(self, group, start=None, end=None,
             satoshis=False,
             ichimoku=False,
             yrange_open_close=False,
             colorup=COLOR_UP, colordown=COLOR_DOWN,
             bar_width_ratio=BAR_WIDTH_RATIO,
             fig_width=FIGURE_WIDTH,
             fig_height=FIGURE_HEIGHT,
             dpi=FIGURE_DPI):
        """
        Generate an OHLC-V plot for given currency history
        :param group: length (in minutes) of the interval per bar
        :param start: (datetime) moment to being plot, default to as far back
            as the data permits
        :param end: (datetime) moment to end plot, default to now
        :param satoshis: display values in satoshis (Base * 10e7)
        :param yrange_open_close: use open/close min/max as the range of the
            y-axis instead of high/low as highs and lows can be outliers that
            may dwarf the effective range
        :param colorup: color of candle when close is higher than open
        :param colordown: color of candle when close is lower than open
        :param bar_width_ratio: ratio of interval that each bar occupies,
            used for aesthetics. Should be betweeen (0, 1), reduce ratio to
            increase padding between bars
        :param fig_width: figure width in inches
        :param fig_height: figure height in inches
        :param dpi: resolution of figure
        """
        resampled_data = self.resample(self.data, group)

        if satoshis:
            btc_vol_tmp = resampled_data[['btc_volume']]
            del resampled_data['btc_volume']
            resampled_data *= SATOSHI_FACTOR
            resampled_data = resampled_data.assign(btc_volume=btc_vol_tmp)

        if ichimoku:
            # Test, do moving avg
            tenkan_sen_series = tenkan_sen(resampled_data)
            kijun_sen_series = kijun_sen(resampled_data)
            senkou_span_a_series = senkou_span_a(tenkan_sen_series,
                                                 kijun_sen_series)
            senkou_span_b_series = senkou_span_b(resampled_data)
            chikou_span_series = chikou_span(resampled_data)
            ichi_df = pd.concat([
                tenkan_sen_series, kijun_sen_series, senkou_span_a_series,
                senkou_span_b_series, chikou_span_series,
            ], axis=1)
            resampled_data = resampled_data.append(ichi_df)

        resampled_data = self.time_span(resampled_data, start, end)

        resampled_quotes = []
        for i, row in resampled_data.iterrows():
            resampled_quotes.append((date2num(i), row.open, row.high, row.low,
                                     row.close))

        # Generate figure
        fig = plt.figure(figsize=(FIGURE_WIDTH, FIGURE_HEIGHT))
        grid_spec = GridSpec(2, 1, height_ratios=[3, 1])
        candle_ax = plt.subplot(grid_spec[0])
        candle_ax.grid(True, linewidth=0.5)
        candle_ax.set_axisbelow(True)
        # Determined by ratio of 1 day
        bar_width = BAR_WIDTH_RATIO * 1 / (MINS_IN_DAY / group)
        candlestick_ohlc(candle_ax, quotes=resampled_quotes, colorup=COLOR_UP,
                         colordown=COLOR_DOWN, width=bar_width)
        if yrange_open_close:
            ymin = min([resampled_data.open.min(), resampled_data.close.min()])
            ymax = max([resampled_data.open.max(), resampled_data.close.max()])
            ybuffer = Y_AXIS_PAD_RATIO * (ymax - ymin)
            candle_ax.set_ylim(max(0, ymin - ybuffer), ymax + ybuffer)

        candle_ax.xaxis_date()
        # candle_ax.autoscale_view()
        candle_ax.xaxis.set_ticklabels([])
        candle_ax.text(0.015, 0.98, self.symbol, ha='left', va='top',
                       fontsize=22, color='#045a8d', weight=900,
                       transform=candle_ax.transAxes)

        if ichimoku:
            candle_ax.plot(resampled_data.tenkan_sen, color=COLOR_TENKAN_SEN,
                           linewidth=1)
            candle_ax.plot(resampled_data.kijun_sen, color=COLOR_KIJUN_SEN,
                           linewidth=1)
            candle_ax.plot(resampled_data.senkou_span_a, color=COLOR_SENKOU_SPAN,
                           linewidth=0.5)
            candle_ax.plot(resampled_data.senkou_span_b, color=COLOR_SENKOU_SPAN,
                           linewidth=0.5)
            candle_ax.fill_between(
                resampled_data.senkou_span_a.index,
                resampled_data.senkou_span_a, resampled_data.senkou_span_b,
                where=resampled_data.senkou_span_a >= resampled_data.senkou_span_b,
                facecolor=COLOR_SENKOU_UP,
                interpolate=False)
            candle_ax.fill_between(
                resampled_data.senkou_span_a.index,
                resampled_data.senkou_span_a, resampled_data.senkou_span_b,
                where=resampled_data.senkou_span_a <= resampled_data.senkou_span_b,
                facecolor=COLOR_SENKOU_DOWN,
                interpolate=False)
            candle_ax.plot(resampled_data.chikou_span, color=COLOR_CHIKOU_SPAN,
                           linewidth=1)
        # candle_ax.set_facecolor('black')
        candle_ax.set_xmargin(0.)

        vol_ax = plt.subplot(grid_spec[1])
        vol_ax.bar([i[0] for i in resampled_quotes],
                   resampled_data.btc_volume,
                   width=bar_width)
        vol_ax.xaxis_date()
        # vol_ax.autoscale_view()
        vol_ax.grid(True, axis='x', linewidth=0.5)
        vol_ax.set_axisbelow(True)
        vol_ax.set_xlim(*candle_ax.get_xlim())

        fig.tight_layout(h_pad=-0.2)
        fig.set_dpi(FIGURE_DPI)

        return fig

    # def figure(self, data, title,
    #            satoshis=False,
    #            yrange_open_close=True,
    #            colorup=COLOR_UP, colordown=COLOR_DOWN,
    #            bar_width_ratio=BAR_WIDTH_RATIO,
    #            fig_width=FIGURE_WIDTH,
    #            fig_height=FIGURE_HEIGHT,
    #            dpi=FIGURE_DPI):
    #     """
    #     Generate matplotlib OHLC-V figure
    #     """
    #     quotes = []
    #     for timestamp, row in data.iterrows():
    #         quotes.append((date2num(timestamp), row.open, row.high, row.low,
    #                        row.close))

    #     # Generate figure
    #     fig = plt.figure(figsize=(FIGURE_WIDTH, FIGURE_HEIGHT))
    #     grid_spec = GridSpec(2, 1, height_ratios=[3, 1])
    #     candle_ax = plt.subplot(grid_spec[0])
    #     # candle_ax.grid(color='r', linewidth=5.5)
    #     candle_ax.set_axisbelow(True)
    #     # Determined by ratio of 1 day
    #     bar_width = BAR_WIDTH_RATIO * 1 / (MINS_IN_DAY / group)
    #     candlestick_ohlc(candle_ax, quotes=quotes, colorup=COLOR_UP,
    #                      colordown=COLOR_DOWN, width=bar_width)
    #     if yrange_open_close:
    #         ymin = min([resampled_data.open.min(),
    #                     resampled_data.close.min()])
    #         ymax = max([resampled_data.open.max(),
    #                     resampled_data.close.max()])
    #         ybuffer = Y_AXIS_PAD_RATIO * (ymax - ymin)
    #         candle_ax.set_ylim(max(0, ymin - ybuffer), ymax + ybuffer)
    #     candle_ax.xaxis_date()
    #     candle_ax.autoscale_view()
    #     candle_ax.xaxis.set_ticklabels([])
    #     candle_ax.set_title(self.symbol)

    #     vol_ax = plt.subplot(grid_spec[1])
    #     vol_ax.bar([i[0] for i in resampled_quotes],
    #                resampled_data.btc_volume,
    #                width=bar_width)
    #     vol_ax.xaxis_date()
    #     vol_ax.autoscale_view()
    #     vol_ax.grid(True, axis='x')
    #     vol_ax.set_axisbelow(True)

    #     fig.tight_layout()
    #     fig.set_dpi(FIGURE_DPI)
