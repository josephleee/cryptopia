"""
Ichimoku Kinko Hyo
Props to: https://en.wikipedia.org/wiki/Ichimoku_Kink%C5%8D_Hy%C5%8D
"""

import numpy as np
import pandas as pd


# Traditional Settings
# TENKAN_SEN_WINDOW = 9
# KIJUN_SEN_WINDOW = 26
# SENKOU_SPAN_B_WINDOW = 52
# SENKOU_SPAN_A_AHEAD = 26
# SENKOU_SPAN_B_AHEAD = 52
# CHIKOU_SPAN_BEHIND = 26

TENKAN_SEN_WINDOW = 20
KIJUN_SEN_WINDOW = 60
SENKOU_SPAN_B_WINDOW = 120
SENKOU_SPAN_A_AHEAD = 60
SENKOU_SPAN_B_AHEAD = 120
CHIKOU_SPAN_BEHIND = 30


def half_range(data, window):
    maxes = data.high.rolling(window).max()
    mins = data.low.rolling(window).min()
    return add_ahead((maxes + mins) / 2)


def add_ahead(series):
    """
    """
    freq = series.index.freq or series.index.inferred_freq
    td = pd.to_timedelta(pd.tseries.frequencies.to_offset(freq))
    new_start = series.index[-1] + td
    new_range = pd.date_range(new_start, periods=SENKOU_SPAN_A_AHEAD,
                              freq=freq)
    return series.append(pd.Series(np.nan, index=new_range))


def tenkan_sen(data):
    """
    :param data: OHLC DataFrame
    """
    series = half_range(data, TENKAN_SEN_WINDOW)
    series.name = 'tenkan_sen'
    return series


def kijun_sen(data):
    """
    :param data: OHLC DataFrame
    """
    series = half_range(data, KIJUN_SEN_WINDOW)
    series.name = 'kijun_sen'
    return series


def senkou_span_a(tenkan_sen_series, kijun_sen_series):
    senkou_span_a_series = (tenkan_sen_series + kijun_sen_series) / 2
    senkou_span_a_series.name = 'senkou_span_a'
    return senkou_span_a_series.shift(SENKOU_SPAN_A_AHEAD)


def senkou_span_b(data):
    senkou_span_b_series = half_range(data, SENKOU_SPAN_B_WINDOW)
    senkou_span_b_series.name = 'senkou_span_b'
    return senkou_span_b_series.shift(SENKOU_SPAN_B_AHEAD)


def chikou_span(data):
    series = add_ahead(data.close.copy())
    series.name = 'chikou_span'
    return series.shift(-CHIKOU_SPAN_BEHIND)
